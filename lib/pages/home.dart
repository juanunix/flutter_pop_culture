import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'dart:io' show Platform;
import 'package:pop_culture/conts.dart';

import 'package:pop_culture/framework/flutter_dina.dart';
import 'package:pop_culture/pages/home/cartelera.dart';
import 'package:pop_culture/pages/home/proximamente.dart';
import 'dart:math' as math;

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  static const _PANEL_HEADER_HEIGHT = 48.0;

  final GlobalKey _backdropKey = GlobalKey(debugLabel: 'Backdrop');
  AnimationController _animationController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  BuildContext _scaffoldContext;
  var visibleNotification = false;

  TabController _controller;
  var tabIndex = 0;

  final tabs = ['CARTELERA', 'PROXIMAMENTE', 'PRECIOS'];

  DinaFramework fr;

  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: tabs.length);
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 300),
      value: 1.0,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _animationController.dispose();
    super.dispose();
  }

  Animation<RelativeRect> _getPanelAnimation(BoxConstraints constraints) {
    final double height = constraints.biggest.height;
    final double top = height - _PANEL_HEADER_HEIGHT;
    final double bottom = -_PANEL_HEADER_HEIGHT;
    return new RelativeRectTween(
      begin: new RelativeRect.fromLTRB(0.0, top, 0.0, bottom),
      end: new RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
    ).animate(new CurvedAnimation(
        parent: _animationController, curve: Curves.linear));
  }

  double get _backdropHeight {
    final RenderBox renderBox = _backdropKey.currentContext.findRenderObject();
    return renderBox.size.height;
  }

  bool get _isPanelVisible {
    final AnimationStatus status = _animationController.status;
    final result = status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
    setState(() {
      visibleNotification = result;
    });
    return result;
  }

  Widget _buildStack(BuildContext context, BoxConstraints constraints) {
    final Animation<RelativeRect> animation = _getPanelAnimation(constraints);
    final ThemeData theme = Theme.of(context);
    return new Container(
      key: _backdropKey,
      color: Colors.white,
      child: new Stack(
        children: <Widget>[
          new Center(
            child: new Text("base"),
          ),
          new PositionedTransition(
              rect: animation,
              child: new Material(
                borderRadius: const BorderRadius.only(
                    topLeft: const Radius.circular(0.0),
                    topRight: const Radius.circular(0.0)),
                elevation: 12.0,
                child: new GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onVerticalDragUpdate: (details) {
                    //detect when the user is draging bottom contenet
                    if (_animationController.isAnimating ||
                        _animationController.status ==
                            AnimationStatus.completed) return;

                    _animationController.value -= details.primaryDelta /
                        (_backdropHeight ?? details.primaryDelta);
                  },
                  onVerticalDragEnd: (details) {
                    if (_animationController.isAnimating ||
                        _animationController.status ==
                            AnimationStatus.completed) {
                      return;
                    }

                    final double flingVelocity =
                        details.velocity.pixelsPerSecond.dy / _backdropHeight;
                    if (flingVelocity < 0.0) {
                      _animationController.fling(
                          velocity: math.max(2.0, -flingVelocity));

                      //change  notification icon
                      setState(() {
                        visibleNotification = false;
                      });
                    } else if (flingVelocity > 0.0) {
                      _animationController.fling(
                          velocity: math.min(-2.0, -flingVelocity));
                    } else {
                      _animationController.fling(
                          velocity:
                              _animationController.value < 0.5 ? -2.0 : 2.0);
                    }
                  },
                  child: new Column(children: <Widget>[
                    new Expanded(
                        child: new Column(
                      children: <Widget>[
                        new Container(
                          width: fr.width,
                          color: Colors.white,
                          child:
                              //inicio tabBar
                              new TabBar(
                            isScrollable: false,
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: ShapeDecoration(
                              shape: const StadiumBorder(
                                    side: BorderSide(
                                      color: Colors.amber,
                                      width: 2.0,
                                    ),
                                  ) +
                                  const StadiumBorder(
                                    side: BorderSide(
                                      color: Colors.transparent,
                                      width: 4.0,
                                    ),
                                  ),
                            ),
                            labelColor: Colors.amber,
                            unselectedLabelColor: Colors.black26,
                            labelStyle: TextStyle(fontWeight: FontWeight.bold),
                            controller: _controller,
                            tabs: List.generate(tabs.length, (index) {
                              return new Tab(text: tabs[index]);
                            }),
                          ),
                          //fin tabBar
                        ),
                        new Expanded(
                            child: new TabBarView(
                          controller: _controller,
                          children: [
                            CarteleraPage(
                              fr: fr,
                            ),
                            ProximamentePage(
                              fr: fr,
                            ),
                            Icon(Icons.directions_transit),
                          ],
                        ))
                        //en tabs
                      ],
                    ))
                  ]),
                ),
              )),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _scaffoldContext = context;
    fr = DinaFramework.init(context);
    return new Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: new AppBar(
        backgroundColor: Colors.white,
        leading: new CupertinoButton(
            padding: EdgeInsets.all(5.0),
            child: new SvgPicture.asset(
              "svg/menu.svg",
              width: 30.0,
              height: 30.0,
              color: const Color(0xFFff0000),
            ),
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
            }),
        actions: <Widget>[
          new Image.asset('images/logo.png', width: fr.wp(50.0)),
          SizedBox(width: fr.wp(4.0)),
          new PopupMenuButton<String>(
              itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
                    const PopupMenuItem<String>(
                        value: 'CINE', child: Text('CINE')),
                    const PopupMenuItem<String>(
                        value: 'DEPORTES', child: Text('DEPORTES')),
                  ],
              child: new Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "CINE",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: fr.ip(2.0),
                          color: const Color(0xFFff0000)),
                    ),
                    Icon(
                      Icons.arrow_drop_down,
                      color: const Color(0xFFff0000),
                    )
                  ],
                ),
              )),
          new CupertinoButton(
              padding: EdgeInsets.all(5.0),
              child: new SvgPicture.asset(
                visibleNotification ? "svg/close.svg" : "svg/notification.svg",
                width: 30.0,
                height: 30.0,
                color: const Color(0xFFff0000),
              ),
              onPressed: () {
                _animationController.fling(
                    velocity: _isPanelVisible ? -1.0 : 1.0);
              }),
        ],
        brightness: Brightness.dark,
        // or use Brightness.dark
        elevation: 0.0,
      ),
      body: new SafeArea(//safeArea adds support to notch devices
          top: true,
          bottom: true,
          child: new LayoutBuilder(builder: _buildStack)),
      drawer: new Drawer(//hamburger menu
        child: new ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(top: 35.0, bottom: 0.0),
              child: new Center(
                child: Column(
                  children: <Widget>[
                    new Container(
                      width: 100.0,
                      height: 100.0,
                      decoration: new BoxDecoration(
                        color: const Color(0xffffffff),
                        image: new DecorationImage(
                          image: new NetworkImage(
                              'https://cdn-images-1.medium.com/fit/c/200/200/0*fxqat08kx3HNYZBD.'),
                          fit: BoxFit.cover,
                        ),
                        borderRadius:
                            new BorderRadius.all(new Radius.circular(50.0)),
                        border: new Border.all(
                          color: Colors.amber,
                          width: 2.0,
                        ),
                      ),
                    ),
                    new Text(
                      "Darwin Morocho",
                      style:
                          TextStyle(color: Colors.white, fontSize: fr.ip(1.8)),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new CupertinoButton(
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new SvgPicture.asset(
                                  "svg/controls.svg",
                                  width: 20.0,
                                  height: 20.0,
                                  color: Colors.white,
                                ),
                                new Text(
                                  " AJUSTES",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: fr.ip(1.4)),
                                ),
                              ],
                            ),
                            onPressed: () {}),
                        new CupertinoButton(
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new SvgPicture.asset(
                                  "svg/logout.svg",
                                  width: 20.0,
                                  height: 20.0,
                                  color: Colors.white,
                                ),
                                new Text(
                                  " SALIR",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: fr.ip(1.4)),
                                ),
                              ],
                            ),
                            onPressed: () {})
                      ],
                    )
                  ],
                ),
              ),
              decoration: BoxDecoration(
                color: const Color(0xFFff0000),
              ),
              margin: EdgeInsets.all(0.0),
            ),
            new FlatButton(
                onPressed: () {},
                child: new Row(
                  children: <Widget>[
                    new SvgPicture.asset(
                      "svg/tickets.svg",
                      width: 30.0,
                      height: 30.0,
                      color: Colors.green,
                    ),
                    new Text(
                      " MIS ENTRADAS",
                      style: TextStyle(
                          color: Colors.black54, fontSize: fr.ip(1.9)),
                    ),
                  ],
                )),
            new FlatButton(
                onPressed: () {},
                child: new Row(
                  children: <Widget>[
                    new SvgPicture.asset(
                      "svg/heart.svg",
                      width: 30.0,
                      height: 30.0,
                      color: Colors.red,
                    ),
                    new Text(
                      " MI LISTA",
                      style: TextStyle(
                          color: Colors.black54, fontSize: fr.ip(1.9)),
                    ),
                  ],
                )),
            new FlatButton(
                onPressed: () {},
                child: new Row(
                  children: <Widget>[
                    new SvgPicture.asset(
                      "svg/prices.svg",
                      width: 30.0,
                      height: 30.0,
                      color: Colors.amber,
                    ),
                    new Text(
                      " PRECIOS",
                      style: TextStyle(
                          color: Colors.black54, fontSize: fr.ip(1.9)),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}

class Notifications extends AnimatedWidget {
  const Notifications({
    Key key,
    Listenable listenable,
  }) : super(key: key, listenable: listenable);

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
  }
}
