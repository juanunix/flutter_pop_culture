import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pop_culture/conts.dart';
import 'package:pop_culture/framework/flutter_dina.dart';
import 'package:pop_culture/models/movie.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:pop_culture/models/upcoming.dart';
import 'package:pop_culture/pages/moviePage.dart';
import 'package:pop_culture/pages/movieDetailPage.dart';
import 'dart:math' as math;

class ProximamentePage extends StatefulWidget {
  final DinaFramework fr;

  const ProximamentePage({Key key, @required this.fr}) : super(key: key);

  @override
  _ProximamentePageState createState() => new _ProximamentePageState();
}

class _ProximamentePageState extends State<ProximamentePage> {
  List<UpcomingMovie> movies = <UpcomingMovie>[];

  ScrollController listController;

  @override
  initState() {
    super.initState();

    listController = new ScrollController();

  }

  @override
  dispose() {
    listController.dispose();
    super.dispose();
  }

  fetchMovies() async {
    var url =
        'https://api.themoviedb.org/3/movie/upcoming?api_key=${themoviedb}&language=es-EC&page=1&region=US';
    print(url);
    final response = await http.get(url);

    if (response.statusCode == 200) {
      var parsed = jsonDecode(response.body);

      for (var movieJSON in parsed['results']) {
        var movie = Movie.fromJson(movieJSON);
        if (movie.backdrop_path != null) {
          movies.add(UpcomingMovie.fromJson(movieJSON));
        }
      }
      return movies;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new FutureBuilder(
            future: fetchMovies(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<UpcomingMovie> sMovies = snapshot.data;
                return new GridView.count(
                  crossAxisCount: 3,
                  mainAxisSpacing: 0.0,
                  crossAxisSpacing: 0.0,
                  childAspectRatio: 2 / 3,
                  children: List.generate(sMovies.length, (index) {
                    final imovie = movies[index];
                    return new CupertinoButton(
                        padding: EdgeInsets.all(2.0),
                        child: new Stack(
                          children: <Widget>[
                            new ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: new SizedBox(
                                width: widget.fr.wp(40.0),
                                height: widget.fr.hp(60.0),
                                child: Hero(
                                    tag: 'movie' + index.toString(),
                                    child: new Material(
                                      child: InkWell(
                                        onTap: () {},
                                        child: Image.network(
                                          themoviedb_img + imovie.poster_path,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    )),
                              ),
                            ),
                            new Positioned(
                                bottom: 0.0,
                                left: 0.0,
                                right: 0.0,
                                child: new ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0),
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.symmetric(vertical: 3.0),
                                    color: const Color(0xFFff0000),
                                    child: new Column(
                                      children: <Widget>[
                                        Text(imovie.title,
                                            maxLines:1,
                                            style: TextStyle(
                                              height: 0.8,
                                                color: Colors.white,
                                                fontSize: widget.fr.ip(1.0))),
                                        Text("Estreno: "+imovie.release_date,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: widget.fr.ip(1.4))),
                                      ],
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                    ),
                                  ),
                                ))
                          ],
                        ),
                        onPressed: () {});
                  }),
                );
              } else if (snapshot.hasError) {
                return new Center(
                  child: Text(snapshot.error.toString()),
                );
              }

              return new Center(
                child: SpinKitDualRing(
                  color: Colors.red,
                  size: 80.0,
                ),
              );
            }));
  }
}
